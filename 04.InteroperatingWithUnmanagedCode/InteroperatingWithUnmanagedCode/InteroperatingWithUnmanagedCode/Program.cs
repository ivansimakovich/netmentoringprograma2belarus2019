﻿using PowerManagementAPI;
using System;
using System.IO;
using System.Linq;

namespace InteroperatingWithUnmanagedCode
{
    public class Program
    {
        private const string PathHibFile = "c:\\hiberfil.sys";

        static void Main(string[] args)
        {
            var powerManager = new PowerManager();
            Console.WriteLine("Last sleep time:");
            Console.WriteLine(powerManager.GetLastSleepTime());
            Console.WriteLine("Last wake time:");
            Console.WriteLine(powerManager.GetLastWakeTime().ToString());
            Console.WriteLine("Is batteryp present");
            Console.WriteLine(powerManager.GetSystemBatteryState().BatteryPresent);
            var systemPowerInformation = powerManager.GetSystemPowerInformation();

            Console.WriteLine("Number Core");
            Console.WriteLine(systemPowerInformation.Length);
            foreach (var item in systemPowerInformation?.ToList().Select(x => x.MhzLimit))
            {
                Console.WriteLine("MhzLimit - The limit on the processor clock frequency, in megahertz.This number is the maximum specified processor " +
                    "clock frequency multiplied by the current processor thermal throttle limit.");
                Console.WriteLine(item);
            }

            Console.WriteLine("Work with hiberfil file:");
            Console.WriteLine(File.Exists(PathHibFile));
            powerManager.DeleteFile();
            Console.WriteLine(File.Exists(PathHibFile));
            powerManager.ReserveFile();
            Console.WriteLine(File.Exists(PathHibFile));


            Console.WriteLine("Let's sleep? IF YOU DON't AGREE CLOSE CONSOLE AND YOU MUSTN'T PRESS ANYKEY!");
            powerManager.SetSuspendState(true, false, false);
            Console.ReadKey();
        }
    }
}
