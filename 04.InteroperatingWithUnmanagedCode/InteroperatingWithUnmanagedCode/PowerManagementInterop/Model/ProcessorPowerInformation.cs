﻿using System.Runtime.InteropServices;

namespace PowerManagementAPI.model
{
    /// <summary>
    /// https://docs.microsoft.com/ru-ru/windows/win32/power/processor-power-information-str
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ProcessorPowerInformation
    {
        public uint Number;
        public uint MaxMhzv;
        public uint CurrentMhz;
        public uint MhzLimit;
        public uint MaxIdleState;
        public uint CurrentIdleState;
    }
}
