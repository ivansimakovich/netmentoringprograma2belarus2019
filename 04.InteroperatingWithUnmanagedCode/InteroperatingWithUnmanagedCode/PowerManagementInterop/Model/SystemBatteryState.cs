﻿using System.Runtime.InteropServices;

namespace PowerManagementAPI
{
    /// <summary>
    /// https://docs.microsoft.com/ru-ru/windows/win32/api/winnt/ns-winnt-system_battery_state
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class SystemBatteryState
    {
        public bool AcOnLine;
        public bool BatteryPresent;
        public bool Charging;
        public bool Discharging;
        public bool Spare1;
        public bool Spare2;
        public bool Spare3;
        public bool Spare4;
        public byte Tag;
        public uint MaxCapacity;
        public uint RemainingCapacity;
        public uint Rate;
        public uint EstimatedTime;
        public uint DefaultAlert1;
        public uint DefaultAlert2;
    }
}
