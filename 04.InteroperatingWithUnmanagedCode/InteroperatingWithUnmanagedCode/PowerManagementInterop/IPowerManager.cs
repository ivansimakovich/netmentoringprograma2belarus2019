﻿using PowerManagementAPI.model;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace PowerManagementAPI
{
    [ComVisible(true)]
    [Guid("A3FD758B-D1D0-499E-8C98-DD1E06EA3BF4")]
    [InterfaceType(ComInterfaceType.InterfaceIsIInspectable)]
    public interface IPowerManager
    {
        DateTime? GetLastSleepTime();

        DateTime? GetLastWakeTime();

        SystemBatteryState GetSystemBatteryState();

        ProcessorPowerInformation[] GetSystemPowerInformation();
    }
}
