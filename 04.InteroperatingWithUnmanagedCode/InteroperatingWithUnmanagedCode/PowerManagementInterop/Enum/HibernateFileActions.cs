﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PowerManagementAPI.Enum
{
    internal enum HibernateFileActions
    {
        Delete = 0,
        Reserve = 1
    }
}
