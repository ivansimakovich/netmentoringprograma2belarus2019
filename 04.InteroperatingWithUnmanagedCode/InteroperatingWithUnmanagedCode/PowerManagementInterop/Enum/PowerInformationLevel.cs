﻿namespace PowerManagementAPI
{
    internal enum PowerInformationLevel
    {
        SystemBatteryState = 5,
        SystemReserveHiberFile = 10,
        ProcessorInformation = 11,
        LastWakeTime = 14,
        LastSleepTime = 15
    }
}