using ExpressionTrees.Task2.ExpressionMapping.Tests.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ExpressionTrees.Task2.ExpressionMapping.Tests
{
    [TestClass]
    public class ExpressionMappingTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Foo, Bar>();

            var fooClass = new Foo() { Id = 12, Name = "TempName" };
            var res = mapper.Map(fooClass);
            Console.WriteLine($"{fooClass.Id} - {fooClass.Name} = is type {fooClass.GetType()}");
            Console.WriteLine($"{res.Id} - {res.Name} = is type {res.GetType()}");
        }
    }
}
