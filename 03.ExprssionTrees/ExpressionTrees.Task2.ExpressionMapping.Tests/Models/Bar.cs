﻿namespace ExpressionTrees.Task2.ExpressionMapping.Tests.Models
{
    internal class Bar
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
