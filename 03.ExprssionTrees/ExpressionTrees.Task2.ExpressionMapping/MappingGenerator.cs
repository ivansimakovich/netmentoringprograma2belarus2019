﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace ExpressionTrees.Task2.ExpressionMapping
{
    public class MappingGenerator
    {
        public Mapper<TInput, TOutput> Generate<TInput, TOutput>()
        {
            var source = Expression.Parameter(typeof(TInput), "source");
            var body = Expression.MemberInit(Expression.New(typeof(TOutput)),
                source.Type.GetProperties().Select(p => Expression.Bind(typeof(TOutput).GetProperty(p.Name), Expression.Property(source, p))));
            var expr = Expression.Lambda<Func<TInput, TOutput>>(body, source);
            return new Mapper<TInput, TOutput>(expr.Compile());
        }
    }
}
