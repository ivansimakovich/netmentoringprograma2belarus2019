﻿/*
 * Create a class based on ExpressionVisitor, which makes expression tree transformation:
 * 1. converts expressions like <variable> + 1 to increment operations, <variable> - 1 - into decrement operations.
 * 2. changes parameter values in a lambda expression to constants, taking the following as transformation parameters:
 *    - source expression;
 *    - dictionary: <parameter name: value for replacement>
 * The results could be printed in console or checked via Debugger using any Visualizer.
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Expression Visitor for increment/decrement.");
            Console.WriteLine();

            var replacementDict = new Dictionary<string, ConstantExpression>
            {
                ["a"] = Expression.Constant(55),
                ["b"] = Expression.Constant(22),
            };

            var x = 5;
            Expression<Func<int, int>> source_exp = (a) => (1 + a) * (a + 1);
            var result_exp = new IncDecExpressionVisitor().VisitAndConvert(source_exp, "");
            Console.WriteLine("Before: {0} = {1}, (a = {2})", source_exp, source_exp.Compile().Invoke(x), x);
            Console.WriteLine("After: {0} = {1}, (a = {2})", result_exp, result_exp.Compile().Invoke(x), x);

            Console.WriteLine();
            Console.WriteLine("After replacing:");
            var result = (LambdaExpression)new IncDecExpressionVisitor(replacementDict).Visit(source_exp);
            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}
