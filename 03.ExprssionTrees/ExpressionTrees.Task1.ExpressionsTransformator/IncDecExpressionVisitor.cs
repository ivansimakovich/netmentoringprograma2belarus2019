﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
    public class IncDecExpressionVisitor : ExpressionVisitor
    {
        private Dictionary<string, ConstantExpression> parameters = new Dictionary<string, ConstantExpression>();
        private int intent;

        public IncDecExpressionVisitor()
        {
        }

        public IncDecExpressionVisitor(Dictionary<string, ConstantExpression> parameters)
        {
            this.parameters = parameters;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (node.NodeType == ExpressionType.Add)
            {
                ParameterExpression param = null;
                ConstantExpression constant = null;
                if (node.Left.NodeType == ExpressionType.Parameter)
                {
                    param = (ParameterExpression)node.Left;
                }
                else if (node.Left.NodeType == ExpressionType.Constant)
                {
                    constant = (ConstantExpression)node.Left;
                }

                if (node.Right.NodeType == ExpressionType.Parameter)
                {
                    param = (ParameterExpression)node.Right;
                }
                else if (node.Right.NodeType == ExpressionType.Constant)
                {
                    constant = (ConstantExpression)node.Right;
                }

                if (param != null && constant != null
                                  && constant.Type == typeof(int)
                                  && (int)constant.Value == 1
                                  && !parameters.TryGetValue(param.Name, out var ce))
                {
                    var rt = Expression.Increment(param);
                    return rt;
                }
            }

            return base.VisitBinary(node);
        }

        protected override Expression VisitParameter(ParameterExpression node)
            => parameters.TryGetValue(node.Name, out var ce) ? (Expression)ce : node;

        protected override Expression VisitLambda<T>(Expression<T> node)
            => Expression.Lambda(Visit(node.Body), node.Parameters);
    }
}
