﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Expressions.Task3.E3SQueryProvider
{
    public class ExpressionToFTSRequestTranslator : ExpressionVisitor
    {
        public List<string> expressions = new List<string>();
        public readonly Dictionary<string, string> _stringExtpatterns = new Dictionary<string, string>
        {
            ["StartsWith"] = "({0}*)",
            ["EndsWith"] = "(*{0})",
            ["Contains"] = "(*{0}*)",
        };
        readonly StringBuilder _resultStringBuilder;
        private bool isNeedListExp;

        public ExpressionToFTSRequestTranslator()
        {
            _resultStringBuilder = new StringBuilder();
        }

        public string Translate(Expression exp)
        {
            Visit(exp);
            return _resultStringBuilder.ToString();
        }

        public List<string> GetQueries(Expression exp)
        {
            isNeedListExp = true;
            Visit(exp);
            return expressions;
        }
        #region protected methods

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable)
                && node.Method.Name == "Where")
            {
                var predicate = node.Arguments[1];
                Visit(predicate);
                return node;
            }

            if (node.Method.DeclaringType == typeof(string)
                && _stringExtpatterns.ContainsKey(node.Method.Name))
            {
                var constant = ((string)((ConstantExpression)node.Arguments[0]).Value).Replace("\\", "");
                var stringConstant = string.Format(_stringExtpatterns[node.Method.Name], constant);
                return base.VisitMethodCall(Expression.Call(node.Object as MemberExpression,
                    node.Method,
                    Expression.Constant(stringConstant)));
            }

            if (node.Method.DeclaringType == typeof(string)
                && node.Method.Name == "Equals")
            {
                return Visit(Expression.Equal(node.Object, node.Arguments[0]));
            }
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:
                    var expressionTypes = new List<ExpressionType>() { ExpressionType.MemberAccess, ExpressionType.Constant };
                    if (!expressionTypes.Contains(node.Left.NodeType))
                        throw new NotSupportedException(string.Format("Left operand should be property or field or constant", node.NodeType));

                    if (!expressionTypes.Contains(node.Right.NodeType))
                        throw new NotSupportedException(string.Format("Right operand should be property or field or constant", node.NodeType));

                    Expression leftPart;
                    Expression rightPart;
                    if (node.Left.NodeType == ExpressionType.MemberAccess)
                    {
                        leftPart =  node.Left;
                        rightPart = node.Right;
                    }
                    else
                    {
                        leftPart =  node.Right;
                        rightPart = node.Left;
                    }

                    Visit(leftPart);
                    _resultStringBuilder.Append("(");
                    Visit(rightPart);
                    _resultStringBuilder.Append(")");
                    break;

                case ExpressionType.AndAlso:
                    if (isNeedListExp)
                    {
                        _resultStringBuilder.Clear();
                        Visit(node.Left);
                        expressions.Add(_resultStringBuilder.ToString());
                        _resultStringBuilder.Clear();
                        Visit(node.Right);
                        expressions.Add(_resultStringBuilder.ToString());
                    }
                    else
                    {
                        Visit(node.Left);
                        _resultStringBuilder.Append(", ")
;                        Visit(node.Right);
                    }
                    break;

                default:
                    throw new NotSupportedException(string.Format("Operation {0} is not supported", node.NodeType));
            };

            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            _resultStringBuilder.Append(node.Member.Name).Append(":");

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
             _resultStringBuilder.Append(node.Value);

            return node;
        }

        #endregion
    }
}
