﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace Debug
{
    class Program
    {
        static void Main(string[] args)
        {

            var networkInterface = ((IEnumerable<NetworkInterface>)NetworkInterface.GetAllNetworkInterfaces()).FirstOrDefault();
            byte[] addressBytes = networkInterface.GetPhysicalAddress().GetAddressBytes();

            var dateBytes = BitConverter.GetBytes(DateTime.Now.Date.ToBinary());

            int length = Math.Min(addressBytes.Length, dateBytes.Length);

            var queue = new Queue<int>();
            for (int i = 0; i < length; i++)
            {
                int value = (dateBytes[i] ^ addressBytes[i]) * 10;
                queue.Enqueue(value);

            }

            string key = string.Join("-", queue);

            System.Console.WriteLine("Key = ");
            System.Console.WriteLine(key);
        }
    }
}
