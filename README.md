Description
The program includes extended .NET topics for skilled developers: Multi-threading, Asynchronous programming, Advanced debugging, Expressions, Messaging and other. Some of the modules are actualized according to modern realities.

Agenda
Module 1: Multi-threading in .NET 

Multi-threading concepts (Threads, ThreadPool, Synchronization in User mode and Kernel mode)
Exception Handling in Multi-threading applications
Task Parallel Library (TPL)
Thread-safe collections
Module 2: Asynchronous programming

async/await concepts (Asynchronous methods, Synchronization Contexts - what happens under the hood) 
Dos and Don'ts in asynchronous programming
Module 3: Expressions, IQueryable

IEnumerable and custom iterators
IQueryable and IQueryProvider
Expression Trees
Structure of simple LINQ provider
Module 4: Interoperating with Unmanaged Code 

Consuming unmanaged DLL functions
Exposing COM Components to the .NET Framework
Exposing .NET Framework Components to COM
Module 5: Message queues 

Introduction to Messaging, pub/sub pattern
One of the following message systems:
AMQP-based messaging (RabbitMQ, Azure Service Bus)
Kafka
MSMQ and System.Messaging
Module 6: Advanced debugging

Debugging tools and practices
Disassembly 
Module 7: Aspect-oriented programming 

Introduction to AOP (Concepts, terms and examples)
AOP Frameworks (Castle.Core, PostSharp, etc.)