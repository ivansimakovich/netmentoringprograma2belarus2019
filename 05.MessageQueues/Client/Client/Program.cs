﻿using System;
using System.Collections.Generic;
using System.IO;
using RabbitMQ.Client;

namespace Client
{
    class Program
    {
        public static void Main()
        {
            Run();
        }

        private static void Run()
        {
            string defaultFolderPath = Directory.GetCurrentDirectory() + @"\Client";

            Console.WriteLine($"Enter directory for observing(by default {defaultFolderPath}):");
            string path = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(path))
            {
                Directory.CreateDirectory(defaultFolderPath);
                path = defaultFolderPath;
            }

            string defaultSearchPattern = $"*.pdf";
            using (FileSystemWatcher watcher = new FileSystemWatcher())
            {
                watcher.Path = path;

                watcher.NotifyFilter = NotifyFilters.LastWrite
                                       | NotifyFilters.FileName;

                watcher.Filter = defaultSearchPattern;

                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;

                watcher.EnableRaisingEvents = true;

                Console.WriteLine("Press 'q' to quit the sample.");
                while (Console.Read() != 'q') ;
            }
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "localhost"
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "scanerQueue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = File.ReadAllBytes(e.FullPath);

                channel.BasicPublish(exchange: "",
                                     routingKey: "scanerQueue",
                                     basicProperties: null,
                                     body: body);
                Console.WriteLine(" [x] Sent {0}", e.FullPath);
            }
        }
    }
}
