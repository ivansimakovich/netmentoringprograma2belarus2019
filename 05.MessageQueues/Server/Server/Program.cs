﻿using System;
using System.IO;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
                          {
                              HostName = "localhost"
                          };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "scanerQueue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    string folderPath = Directory.GetCurrentDirectory() + @"\Server";
                    Directory.CreateDirectory(folderPath);
                    var name = DateTime.Now.ToString().Replace('/', '_').Replace(' ', '_').Replace(':', '_');
                    string fileNamePath = folderPath + '\\' + name + ".pdf";
                    File.WriteAllBytes(fileNamePath, body);
                    Console.WriteLine(" [x] Received {0}", ea.RoutingKey);
                };
                channel.BasicConsume(queue: "scanerQueue",
                                     autoAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
    }
}
