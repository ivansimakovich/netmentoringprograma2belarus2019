﻿using Autofac;
using CodeRewriting;
using PowerManagementAPI.Interfaces;
using ILogger = Logging.ILogger;

namespace AOP
{
    public interface IExecutor
    {
        void StartApp();
    }

    public class Executor : IExecutor
    {
        private readonly IFullfill _fullfiller;
        private readonly ILogger _logger;

        [LoggerAspect]
        public Executor(IContainer ioc)
        {
            _logger = ioc.Resolve<ILogger>();
            _fullfiller = ioc.Resolve<IFullfill>();
        }

        [LoggerAspect]
        public void StartApp()
        {
            _fullfiller.Run();
        }
    }
}
