﻿using System;
using Autofac;
using Autofac.Extras.DynamicProxy;
using Castle.DynamicProxy;
using DynamicProxy;
using NLog;
using NLog.Config;
using NLog.Targets;
using PowerManagementAPI;
using PowerManagementAPI.Interfaces;
using Logger = Logging.Logger;

namespace AOP
{
    class Program
    {
        private const string DefaultPath = @"C:\winserv\worker.log";
        private const bool UseCodeRewritingLogs = false;
        private const bool UseDynamicProxyLogs = true;

        private static void Main(string[] args)
        {
            var logFactory = GetLogFactory(DefaultPath);

            var logger = Logger.Current;

            logger.SetActualLogger(logFactory.GetLogger("AOP.PowerManager"), UseCodeRewritingLogs);


            logger.LogInfo("Main start");

            var container = GetContainer(UseDynamicProxyLogs);

            var serv = new Executor(container);

            try
            {
                serv.StartApp();
            }
            catch (Exception e)
            {
                if (logger != null)
                {
                    logger.LogError(e);
                }
            }
        }

        private static LogFactory GetLogFactory(string path)
        {

            string logPath = path;
            var logConfig = new LoggingConfiguration();

            var target = new FileTarget()
            {
                Name = "Def",
                FileName = logPath,
                Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
            };

            logConfig.AddTarget(target);
            logConfig.AddRuleForAllLevels(target);
            var consoleTarget = new ConsoleTarget
            {
                Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}",
                Name = "console"
            };


            logConfig.AddTarget(consoleTarget);
            logConfig.AddRuleForAllLevels(consoleTarget);

            var logFactory = new LogFactory(logConfig);

            return logFactory;
        }

        private static IContainer GetContainer(bool useDynamicProxyLogs)
        {
            var builder = new ContainerBuilder();

            builder.Register(c => new LoggerInterceptor(useDynamicProxyLogs)).Named<IInterceptor>("logger-interceptor");

            builder.RegisterType<Fullfill>().As<IFullfill>().EnableInterfaceInterceptors().InterceptedBy("logger-interceptor");
            builder.RegisterType<PowerManager>().As<IPowerManager>().EnableInterfaceInterceptors().InterceptedBy("logger-interceptor");

            builder.RegisterInstance(Logger.Current).As<Logging.ILogger>();

            var container = builder.Build();
            return container;
        }
    }
}
