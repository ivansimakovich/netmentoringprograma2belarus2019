﻿namespace Logging.Serialization
{
    public interface ISerializer
    {
        string Serialize(object arg);
    }
}
