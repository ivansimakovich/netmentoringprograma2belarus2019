﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using PowerManagementAPI.Enum;
using PowerManagementAPI.model;
using CodeRewriting;
using PowerManagementAPI.Interfaces;

namespace PowerManagementAPI
{
    [ComVisible(true)]
    [Guid("AA0170C3-D083-4548-AA59-B6FBA423F7FE")]
    [ClassInterface(ClassInterfaceType.None)]
    public class PowerManager : IPowerManager
    {
        public PowerManager()
        {
        }

        private DateTime GetLastStartUpTime()
        {
            var upTime = new PerformanceCounter("System", "System Up Time");
            upTime.NextValue();
            var timeSpanB = TimeSpan.FromSeconds(upTime.NextValue());
            return DateTime.Now + timeSpanB;
        }

        [LoggerAspect]
        public DateTime? GetLastSleepTime()
        {
            long lastSleepTimeTicks = GetStructure<long>(PowerInformationLevel.LastSleepTime);
            if (!Convert.ToBoolean(lastSleepTimeTicks))
            {
                return null;
            }

            DateTime bootUpTime = GetLastStartUpTime();
            DateTime lastSleepTime = bootUpTime.AddTicks(lastSleepTimeTicks);
            return lastSleepTime;
        }

        [LoggerAspect]
        public DateTime? GetLastWakeTime()
        {
            long lastWakeTimeTicks = GetStructure<long>(PowerInformationLevel.LastWakeTime);

            if (!Convert.ToBoolean(lastWakeTimeTicks))
            {
                return null;
            }

            DateTime bootUpTime = GetLastStartUpTime();
            DateTime lastWakeTime = bootUpTime.AddTicks(lastWakeTimeTicks);
            return lastWakeTime;
        }

        [LoggerAspect]
        public SystemBatteryState GetSystemBatteryState()
        {
            var batteryState = GetStructure<SystemBatteryState>(PowerInformationLevel.SystemBatteryState);

            return batteryState;
        }

        [LoggerAspect]
        public ProcessorPowerInformation[] GetSystemPowerInformation()
        {
            var procCount = Environment.ProcessorCount;
            var procInfo = new ProcessorPowerInformation[procCount];
            var retval = PowerManagementInterop.CallNtPowerInformation(
                (int)PowerInformationLevel.ProcessorInformation,
                IntPtr.Zero,
                0,
                procInfo,
                procInfo.Length * Marshal.SizeOf<ProcessorPowerInformation>()
                );

            if (Convert.ToBoolean(retval))
            {
                throw new Win32Exception();
            }

            return procInfo;
        }

        public void ReserveFile()
        {
            HibernateFileAction(HibernateFileActions.Reserve);

        }

        public void DeleteFile()
        {
            HibernateFileAction(HibernateFileActions.Delete);

        }

        public void SetSuspendState(bool hibernate, bool forceCritical, bool disableWakeEvent)
        {
            uint result = PowerManagementInterop.SetSuspendState(hibernate, forceCritical, disableWakeEvent);

            if (result == 0)
            {
                throw new Win32Exception();
            }
        }

        private void HibernateFileAction(HibernateFileActions fileActions)
        {
            int intSize = Marshal.SizeOf<bool>();
            IntPtr intPtr = Marshal.AllocCoTaskMem(intSize);
            Marshal.WriteByte(intPtr, (byte)fileActions);

            var retval = PowerManagementInterop.CallNtPowerInformation(
                (int)PowerInformationLevel.SystemReserveHiberFile,
                intPtr,
                intSize,
                IntPtr.Zero,
                0);
            Marshal.FreeHGlobal(intPtr);
        }

        private T GetStructure<T>(PowerInformationLevel informationLevel)
        {
            var infoLevel = (int)informationLevel;
            IntPtr inputBuffer = IntPtr.Zero;
            int inputBufSize = 0;
            int outputBufSize = Marshal.SizeOf<T>();
            IntPtr outputBuffer = Marshal.AllocCoTaskMem(outputBufSize);

            var isErrorOccured = PowerManagementInterop.CallNtPowerInformation(
                infoLevel,
                inputBuffer,
                inputBufSize,
                outputBuffer,
                outputBufSize);

            Marshal.FreeHGlobal(inputBuffer);

            if (!Convert.ToBoolean(isErrorOccured))
            {
                var result = Marshal.PtrToStructure<T>(outputBuffer);
                Marshal.FreeHGlobal(outputBuffer);
                return result;
            }
            else
            {
                Marshal.FreeHGlobal(outputBuffer);
                throw new Win32Exception();
            }
        }
    }
}
