﻿using PowerManagementAPI.model;
using System;
using System.Runtime.InteropServices;

namespace PowerManagementAPI
{
    internal class PowerManagementInterop
    {
        [DllImport("PowrProf.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            int informaitonLevel,
            IntPtr inputBuffer,
            int inputBufSize,
            IntPtr outputBuffer,
            int outputBufferSize);

        [DllImport("PowrProf.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            int informationLevel,
            IntPtr lpInputBuffer,
            int inputBufSize,
            [Out] ProcessorPowerInformation[] lpOutputBuffer,
            int nOutputBufferSize);

        [DllImport("PowrProf.dll", SetLastError = true)]
        public static extern uint SetSuspendState(
            bool Hibernate,
            bool ForceCritical,
            bool DisableWakeEvent);
    }
}
